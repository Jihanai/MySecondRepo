from django.shortcuts import render
from datetime import date

# Enter your name here
mhs_name = 'Jihan Amalia Irfani'
mhs_thlahir = 1998

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(mhs_thlahir)}
    return render(request, 'index.html', response)
    
# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    now = date.today()
    age = now.year - birth_year
    return age

    #pass
