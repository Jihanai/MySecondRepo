from django.apps import AppConfig


class LabPpwConfig(AppConfig):
    name = 'lab_ppw'
